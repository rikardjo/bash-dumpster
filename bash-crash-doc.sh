#!/bin/bash

# @language BASH

# INTERNAL VARIABLES

# $BASH The path to the Bash binary itself 
# $BASH_SUBSHELL A variable indicating the subshell level.
# $BASHPID Process ID of the current instance of Bash.
# $EUID "effective" user ID number
# $FUNCNAME Name of the current function
# $GLOBIGNORE A list of filename patterns to be excluded from matching in globbing.
# $GROUPS This is a listing (array) of the group id numbers for current user.
# $HOME Home directory of the user, usually /home/username.
# $HOSTNAME The hostname command assigns the system host name at bootup in an init script.
# $HOSTTYPE Like $MACHTYPE, identifies the system hardware.
# $IFS Internal field separator, determines how Bash recognizes fields, or word boundaries, when it interprets character strings.
# $OLDPWD Old working directory ("OLD-Print-Working-Directory", previous directory you were in).
# $OSTYPE Operating system type
# $PATH Path to binaries, usually /usr/bin/, /usr/X11R6/bin/, /usr/local/bin, etc.
# $PIPESTATUS Array variable holding exit status(es) of last executed foreground pipe.
# $PPID The $PPID of a process is the process ID (pid) of its parent process.
# $PROMPT_COMMAND Holds a command to be executed just before the primary prompt, $PS1 is to be displayed.
# $$PS1 This is the main prompt, seen at the command-line.
# $PS2 The secondary prompt, seen when additional input is expected. It displays as ">".
# $PS3 The tertiary prompt, displayed in a select loop (see Example 11-30).
# $PS4 The quartenary prompt, shown at the beginning of each line of output when invoking a script with the -x [verbose trace] option. It displays as "+".
# $PWD Working directory (directory you are in at the time)
# $REPLY The default value when a variable is not supplied to read.
# $SECONDS The number of seconds the script has been running.
# $UID User ID number


# POSITIONAL PARAMETERS

# $0, $1, $2, etc. Positional parameters, passed from command line to script, passed to a function, or set to a variable.
# $# Number of command-line arguments or positional parameters
# $* All of the positional parameters, seen as a single word ("$*" must be quoted)
# $@ Same as $*, but each parameter is a quoted string, that is, the parameters are passed on intact, without interpretation or expansion. This means, among other things, that each parameter in the argument list is seen as a separate word.


# SPECIAL PARAMETERS

# $- Flags passed to script (using set)
# $! PID (process ID) of last job run in background
# $_ Special variable set to final argument of previous command executed.
# $? Exit status of a command, function, or the script itself
# $$ Process ID (PID) of the script itself. The $$ variable often finds use in scripts to construct "unique" temp file names. This is usually simpler than invoking mktemp.

# @ EXPRESSIONS

# [ -a FILE ] True if FILE exists.
# [ -b FILE ] True if FILE exists and is a block-special file.
# [ -c FILE ] True if FILE exists and is a character-special file.
# [ -d FILE ] True if FILE exists and is a directory.
# [ -e FILE ] True if FILE exists.
# [ -f FILE ] True if FILE exists and is a regular file.
# [ -g FILE ] True if FILE exists and its SGID bit is set.
# [ -h FILE ] True if FILE exists and is a symbolic link.
# [ -k FILE ] True if FILE exists and its sticky bit is set.
# [ -p FILE ] True if FILE exists and is a named pipe (FIFO).
# [ -r FILE ] True if FILE exists and is readable.
# [ -s FILE ] True if FILE exists and has a size greater than zero.
# [ -t FD ]	  True if file descriptor FD is open and refers to a terminal.
# [ -u FILE ] True if FILE exists and its SUID (set user ID) bit is set.
# [ -w FILE ] True if FILE exists and is writable.
# [ -x FILE ] True if FILE exists and is executable.
# [ -O FILE ] True if FILE exists and is owned by the effective user ID.
# [ -G FILE ] True if FILE exists and is owned by the effective group ID.
# [ -L FILE ] True if FILE exists and is a symbolic link.
# [ -N FILE ] True if FILE exists and has been modified since it was last read.
# [ -S FILE ] True if FILE exists and is a socket.
# [ FILE1 -nt FILE2 ] True if FILE1 has been changed more recently than FILE2, or if FILE1 exists and FILE2 does not.
# [ FILE1 -ot FILE2 ] True if FILE1 is older than FILE2, or is FILE2 exists and FILE1 does not.
# [ FILE1 -ef FILE2 ] True if FILE1 and FILE2 refer to the same device and inode numbers.
# [ -o OPTIONNAME ]	True if shell option "OPTIONNAME" is enabled.
# [ -z STRING ]	True of the length if "STRING" is zero.
# [ -n STRING ] or [ STRING ]	True if the length of "STRING" is non-zero.
# [ STRING1 == STRING2 ] True if the strings are equal. "=" may be used instead of "==" for strict POSIX compliance.
# [ STRING1 != STRING2 ] True if the strings are not equal.
# [ STRING1 < STRING2 ] True if "STRING1" sorts before "STRING2" lexicographically in the current locale.
# [ STRING1 > STRING2 ] True if "STRING1" sorts after "STRING2" lexicographically in the current locale.
# [ ARG1 OP ARG2 ] where ARG1/ARG2 is integers, and OP is either:
#   -eq which means ==  (equal to)
#   -ne which means !=  (not equal to)
#   -gt which means >   (greater then)
#   -lt which means <   (lesser then)
#   -ge which means >=  (greater then or equal to)
#   -le which means <=  (less then or equal to)

# WILDCARDS

# ? (question mark) this can represent any single character.
# * (asterisk) this can represent any number of characters (including zero, in other words, zero or more characters)
# [ ] (square brackets) specifies a range. If you did m[a,o,u]m it can become: mam, mum, mom if you did: m[a-d]m it can become anything that starts and ends with m and has any character a to d inbetween.
# { } (curly brackets) terms are separated by commas and each term must be the name of something or a wildcard. This wildcard will copy anything that matches either wildcard(s), or exact name(s) (an “or” relationship, one or the other).
# [!] This construct is similar to the [ ] construct, except rather than matching any characters inside the brackets, it'll match any character, as long as it is not listed between the [ and ]. This is a logical NOT.

# REGULAR EXPRESSIONS

# . (dot) will match any single character, equivalent to ? (question mark) in standard wildcard expressions. Thus, "m.a" matches "mpa" and "mea" but not "ma" or "mppa". 
# \ (backslash) is used as an "escape" character, i.e. to protect a subsequent special character. Thus, "\\" searches for a backslash. Note you may need to use quotation marks and backslash(es).
# .* (dot and asterisk) is used to match any string, equivalent to * in standard wildcards.
# * (asterisk) The proceeding item is to be matched zero or more times. ie. n* will match n, nn, nnnn, nnnnnnn but not na or any other character.
# ^ (caret) Means "the beginning of the line". So "^a" means find a line starting with an "a".
# $ (dollar sign) means "the end of the line". So "a$" means find a line ending with an "a".
# [ ] (square brackets) Specifies a range. If you did m[a,o,u]m it can become: mam, mum, mom if you did: m[a-d]m it can become anything that starts and ends with m and has any character a to d inbetween.
# | This wildcard makes a logical OR relationship between wildcards. This way you can search for something or something else (possibly using two different regular expressions). You may need to add a '\' (backslash) before this command to work, because the shell may attempt to interpret this as a pipe.
# [^] This is the equivalent of [!] in standard wildcards. This performs a logical “not”. This will match anything that is not listed within those square brackets.

# CHARACTER CATEGORIES BY POSIX STANDARD
# These can be used by most commands which handle text

# [:upper:] uppercase letters
# [:lower:] lowercase letters
# [:alpha:] alphabetic (letters) meaning upper+lower (both uppercase and lowercase letters)
# [:digit:] numbers in decimal, 0 to 9
# [:alnum:] alphanumeric meaning alpha+digits (any uppercase or lowercase letters or any decimal digits)
# [:space:] whitespace meaning spaces, tabs, newlines and similar
# [:graph:] graphically printable characters excluding space
# [:print:] printable characters including space
# [:punct:] punctuation characters meaning graphical characters minus alpha and digits
# [:cntrl:] control characters meaning non-printable characters
# [:xdigit:] characters that are hexadecimal digits. 

# ASSIGNMENT

# =     (Equal sign) All-purpose assignment operator, which works for both arithmetic and string assignments. No spaces allowed after the "=".

# ARITHMETIC OPERATORS
# Arithmetic operators often occur in an expr or let expression.

# +     plus
# -     minus
# *     multiplication
# /     division
# **    exponentiation
# %     modulo, or mod (returns the remainder of an integer division operation)
# +=    plus-equal (increment variable by a constant)
# -=    minus-equal (decrement variable by a constant)
# *=    times-equal (multiply variable by a constant)
# /=    slash-equal (divide variable by a constant)
# %=    mod-equal (remainder of dividing variable by a constant)



# BITWISE OPERATORS

# <<    bitwise left shift (multiplies by 2 for each shift position)
# <<=   Left-shift-equal (let "var <<= 2" results in var left-shifted 2 bits (multiplied by 4))
# >>    bitwise right shift (divides by 2 for each shift position)
# >>=   right-shift-equal (inverse of <<=)
# &     bitwise AND
# &=    bitwise AND-equal
# |     bitwise OR
# |=    bitwise OR-equal
# ~     bitwise NOT
# ^     bitwise XOR
# ^=    bitwise XOR-equal


# LOGICAL OPERATORS

# ! NOT     Example: if [ ! -f $FILENAME ]; then...
# && AND    Example: if [ $condition1 ] && [ $condition2 ]; then ...
# || OR     Example: if [ $condition1 ] || [ $condition2 ]; then ...


# NUMERICAL CONSTANTS

# Decimal: Number as is.
# Octal: Numbers preceded with 0 (zero) Example: 032 equal 26.
# hexadecimal: Numbers preceded with 0x or 0X, Example: 0x32 equal 50.  

# To convert numbers to another base, preced number with n# before number. Where n is an integer between 2 and 64.
# Example echo $(( 2#111 )) equal decimal 7.
# Another Example: let "b64 = 64#@_"; echo "base-64 number = $b64"; which equal 4031 in decimal.

IFS=" "
ARRAY=()
i=-1
max=100

# an application loop
function main() {

    (( ++i >= max )) && return 1

    # bitwise check if $i is an odd
	# add value of $i in base15 to array
    (( $i & 1 )) \
        && ARRAY[$i]=$i \
        || ARRAY[$i]=$(($i+15#$i)) 
    return 0
}


# iterate by getting a false return value from calling main
while main; do

    sleep .1

done

echo ${ARRAY[@]}
