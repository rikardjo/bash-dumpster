#!/bin/bash
# vim: se ts=4 :

# @pos 1 string
# @pos 2+ strings to compare against
# @ret true on one match, false on none|disambiguate match
match() {

        local w input="${1,,}" disa=();
        local len=${#input}; # needed for uppercase hints
        shift;

		for w in $*; do
			[[ "$input" == "$w" ]] && return 0;
			[[ "$w" == "$input"* ]] && disa+=($w);
		done

        if ! (( ${#disa[*]} == 1 )); then
            printf "Matches: "
            for w in ${disa[*]}; do
                printf "$( echo "${w:0:$len}" | tr '[:lower:]' '[:upper:]')${w:${len}} ";
            done
            echo "";
            return 1;
        fi

        return 0;
}

# test scenario
cmds="some something sometimes any anywhere anyway"

while true; do
	read -p"> " cmd
	test -z "$cmd" && exit 1;
	match $cmd $cmds && echo "$cmd is valid";
done
